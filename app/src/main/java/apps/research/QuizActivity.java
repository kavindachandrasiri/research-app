package apps.research;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class QuizActivity extends AppCompatActivity {

    LinearLayout cv, cvE;
    TextView yes, no, tvQuiz, start, submit, extra, yesE, noE, tvQuizE;
    Handler handler;
    ArrayList<String> array, arrayExtra;
    ArrayList<String> result;
    Random rand;
    int now = 0;
    int nowE = 0;
    int applyExtra = 0;
    JSONArray jsonArray=new JSONArray();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        Intent intent = getIntent();
        String category = intent.getExtras().getString("value");

        cv = findViewById(R.id.card);
        yes = findViewById(R.id.tvOk);
        no = findViewById(R.id.tvNo);
        cvE = findViewById(R.id.cardExtra);
        yesE = findViewById(R.id.tvOkExtra);
        noE = findViewById(R.id.tvNoExtra);
        tvQuizE = findViewById(R.id.tvQuizExtra);
        start = findViewById(R.id.start);
        extra = findViewById(R.id.startExtra);
        submit = findViewById(R.id.submit);
        tvQuiz = findViewById(R.id.tvQuiz);
        handler = new Handler();
        rand = new Random();
        array = new ArrayList<>();
        arrayExtra = new ArrayList<>();
        result = new ArrayList<>();

        if (category.equals("1")) {
            addQuiz1();
            addExtraQuiz1();
        } else if (category.equals("2")) {
            addQuiz2();
            addExtraQuiz2();
        } else {
            addQuiz3();
            addExtraQuiz3();
        }

        tvQuiz.setText(array.get(now).split("\\.")[1]);
        tvQuizE.setText(arrayExtra.get(nowE));

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start.setVisibility(View.GONE);
                cv.setVisibility(View.VISIBLE);
            }
        });

        extra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                extra.setVisibility(View.GONE);
                cvE.setVisibility(View.VISIBLE);
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendData();
            }
        });

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // int randomNum = rand.nextInt((10 - 1) + 1) + 1;
                JSONObject obj=new JSONObject();

                if (now < 9) {
                    result.add(tvQuiz.getText().toString());
                    try {
                        obj.put("question_id",array.get(now).split("\\.")[0]);
                        obj.put("answer","yes");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    now++;
                    applyExtra++;
                    closeAnim(cv);
                    initAnim();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            tvQuiz.setText(array.get(now).split("\\.")[1]);
                            comeAnim(cv);
                        }
                    }, 600);
                } else {
                    if (applyExtra == 5) {
                        try {
                            obj.put("question_id",array.get(now).split("\\.")[0]);
                            obj.put("answer","yes");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        cv.setVisibility(View.GONE);
                        extra.setVisibility(View.VISIBLE);
                        Toast.makeText(QuizActivity.this, "We have extra questions for you", Toast.LENGTH_LONG).show();
                    } else {
                        try {
                            obj.put("question_id",array.get(now).split("\\.")[0]);
                            obj.put("answer","yes");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        cv.setVisibility(View.GONE);
                        submit.setVisibility(View.VISIBLE);
                    }

                }
                jsonArray.put(obj);
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                JSONObject obj=new JSONObject();
                if (now < 9) {
                    result.add("no");
                    try {
                        obj.put("question_id",array.get(now).split("\\.")[0]);
                        obj.put("answer","no");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    now++;
                    closeAnim(cv);
                    initAnim();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            tvQuiz.setText(array.get(now).split("\\.")[1]);
                            comeAnim(cv);
                        }
                    }, 600);
                } else {
                    if (applyExtra == 5) {
                        try {
                            obj.put("question_id",array.get(now).split("\\.")[0]);
                            obj.put("answer","no");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        cv.setVisibility(View.GONE);
                        extra.setVisibility(View.VISIBLE);
                        Toast.makeText(QuizActivity.this, "We have extra questions for you", Toast.LENGTH_LONG).show();
                    } else {
                        try {
                            obj.put("question_id",array.get(now).split("\\.")[0]);
                            obj.put("answer","no");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        cv.setVisibility(View.GONE);
                        submit.setVisibility(View.VISIBLE);
                    }
                }
                jsonArray.put(obj);
            }
        });

        yesE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject obj=new JSONObject();
                if (nowE < 2) {
                    result.add(tvQuizE.getText().toString());
                    try {
                        obj.put("question_id",arrayExtra.get(nowE).split("\\.")[0]);
                        obj.put("answer","yes");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    nowE++;
                    closeAnim(cvE);
                    initAnim();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            tvQuizE.setText(arrayExtra.get(nowE));
                            comeAnim(cvE);
                        }
                    }, 600);
                } else {
                    try {
                        obj.put("question_id",arrayExtra.get(nowE).split("\\.")[0]);
                        obj.put("answer","yes");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    cvE.setVisibility(View.GONE);
                    submit.setVisibility(View.VISIBLE);
                }
                jsonArray.put(obj);
            }
        });

        noE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject obj=new JSONObject();
                if (nowE < 2) {
                    result.add("no");
                    try {
                        obj.put("question_id",arrayExtra.get(nowE).split("\\.")[0]);
                        obj.put("answer","no");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    nowE++;
                    closeAnim(cvE);
                    initAnim();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            tvQuizE.setText(arrayExtra.get(nowE));
                            comeAnim(cvE);
                        }
                    }, 600);
                } else {
                    try {
                        obj.put("question_id",arrayExtra.get(nowE).split("\\.")[0]);
                        obj.put("answer","no");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    cvE.setVisibility(View.GONE);
                    submit.setVisibility(View.VISIBLE);
                }
                jsonArray.put(obj);
            }
        });
    }

    private void addQuiz1() {
        array.add("1.Do you rue even for a minor matter ?");
        array.add("2.Do you always get angry ?");
        array.add("3.Do you think that no body loves and cares you?");
        array.add("4.Do you always suffering from a headache or paining or burning physically ?");
        array.add("5.Without any reasonable reason, do you feel tired or numb ?");
        array.add("6.Does your weight got decreased recently ?");
        array.add("7.Does your motivation to do your educational work,job,household work got reduced ?");
        array.add("8.Do you feel any difficulty when u go to sleep at night ?");
        array.add("9.Do you feel sad without any reason ?");
        array.add("10.Did you think about suiciding recently ?");
    }

    private void addExtraQuiz1() {
        arrayExtra.add("11.Do you rue even for a minor matter ?");
        arrayExtra.add("12.Do you always get angry ?");
        arrayExtra.add("13.Do you think that no body loves and cares you?");
    }

    private void addQuiz2() {
        array.add("2.Do you always get angry ?");
        array.add("4.Do you always suffering from a headache or paining or burning physically ?");
        array.add("10.Did you think about suiciding recently ?");
        array.add("3.Do you think that no body loves and cares you?");
        array.add("6.Does your weight got decreased recently ?");
        array.add("9.Do you feel sad without any reason ?");
        array.add("1.Do you rue even for a minor matter ?");
        array.add("8.Do you feel any difficulty when u go to sleep at night ?");
        array.add("7.Does your motivation to do your educational work,job,household work got reduced ?");
        array.add("5.Without any reasonable reason, do you feel tired or numb ?");
    }

    private void addExtraQuiz2() {
        arrayExtra.add("13.Do you think that no body loves and cares you?");
        arrayExtra.add("11.Do you rue even for a minor matter ?");
        arrayExtra.add("12.Do you always get angry ?");
    }


    private void addQuiz3() {
        array.add("1.Do you rue even for a minor matter ?");
        array.add("5.Without any reasonable reason, do you feel tired or numb ?");
        array.add("9.Do you feel sad without any reason ?");
        array.add("2.Do you always get angry ?");
        array.add("6.Does your weight got decreased recently ?");
        array.add("10.Did you think about suiciding recently ?");
        array.add("3.Do you think that no body loves and cares you?");
        array.add("8.Do you feel any difficulty when u go to sleep at night ?");
        array.add("7.Does your motivation to do your educational work,job,household work got reduced ?");
        array.add("4.Do you always suffering from a headache or paining or burning physically ?");
    }

    private void addExtraQuiz3() {
        arrayExtra.add("11.Do you rue even for a minor matter ?");
        arrayExtra.add("13.Do you think that no body loves and cares you?");
        arrayExtra.add("12.Do you always get angry ?");
    }

    public void comeAnim(View view) {
        view.animate().translationY(0).translationX(0).alpha(1).setDuration(500).setInterpolator(new AccelerateDecelerateInterpolator());
    }

    public void closeAnim(View view) {
        // cv.animate().alpha(0).setDuration(500).setInterpolator(new AccelerateDecelerateInterpolator());
        view.animate().translationY(1500).translationX(0).alpha(1).setDuration(500).setInterpolator(new AccelerateDecelerateInterpolator());
    }

    public void initAnim() {
        cv.setTranslationX(-1500);
        cv.setTranslationY(0);
//        cvE.setTranslationX(-1500);
//        cvE.setTranslationY(0);
    }

    private void jsonParse(String sentence) {

        RequestQueue queue = Volley.newRequestQueue(this);
        String url = null;
        try {
            url = "http://35.231.164.226:8080/depLevel?sentences=" + URLEncoder.encode(sentence, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        final ProgressDialog pd = new ProgressDialog(QuizActivity.this);
        pd.setMessage("Loading...");
        pd.show();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String stressLevel = response.getString("dep_level");
                            pd.dismiss();

                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                pd.dismiss();
            }
        });
        queue.add(request);
    }

    private void sendData() {

        final String[] level = {""};
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://35.231.164.226:8080/stress/questions";

        final ProgressDialog pd = new ProgressDialog(QuizActivity.this);
        pd.setMessage("Loading...");
        pd.show();

        try {
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST, url, jsonArray, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    pd.dismiss();
                    try {
                        JSONObject object = response.getJSONObject(0);
                        level[0] = object.getString("stress");
                        Log.e("level", level[0]);

                        Intent intent = new Intent(QuizActivity.this, ResultActivity.class);
                        intent.putExtra("value", level[0].trim());
                        startActivity(intent);
                        finish();

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(QuizActivity.this,"Something went wrong.please go back and try again...",Toast.LENGTH_LONG).show();
                    pd.dismiss();
                    Log.e("level", error.getMessage());
                }
            });
            queue.add(jsonArrayRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    };

    @Override
    public void onBackPressed() {
        finish();
    }
}
