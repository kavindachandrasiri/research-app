package apps.research;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    String stress;
    TextView tvImage,tvLevel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        Intent intent = getIntent();
        stress = intent.getExtras().getString("value");

        tvImage = findViewById(R.id.tvImage);
        tvLevel= findViewById(R.id.tvLevel);
        tvLevel.setText("Your in the situation of"+System.getProperty("line.separator")+stress+"."+System.getProperty("line.separator")+" No matter to be confused."+System.getProperty("line.separator")+"let's get relaxed"+System.getProperty("line.separator")+ System.getProperty("line.separator")+"Thank You");
        Log.e("strss R",stress);
        if (stress.equals("Least Depression")) {
            tvImage.setBackgroundResource(R.drawable.r1);
        } else if (stress.equals("Highest Depression")) {
            tvImage.setBackgroundResource(R.drawable.r2);
        } else {
            tvImage.setBackgroundResource(R.drawable.r3);
        }

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(ResultActivity.this,MainActivity.class));
        finish();
    }
}
